Projekt semestralny z przedmiotu PIK

Michał Smoła, Marcin Hałas, Kuba Jałowiec, JN

Sciaganie do IntelliJ (dziala w wersji IntelliJ IDEA 2018.1.2 (Community Edition)

1. File -> New -> Project from Version Control -> Git i ustawiamy adres: https://bitbucket.org/pik2018/borowik.git oraz wybieramy folder, do ktorego chcemy zaladowac projekt.
2. Run -> Edit Configurations... -> klikamy na zielony plus po lewej stronie -> Gradle -> Gradle project (rozwijamy przez [...]) -> wybieramy folder, w ktorym jest sciagniety projekt
3. File -> Project structure -> Project SDK -> wybieramy co chcemy 
4. Do debugowania na wlasnej maszynie potrzebujemy serwera Tomcat, ktory sciagamy ze strony https://tomcat.apache.org/download-90.cgi
5. Run -> Edit Configurations... -> klikamy na zielony plus po lewej stronie -> TomcatServer -> Local
6. Server -> Application Server -> Configure -> znow zielony plus po lewej stronie -> wskazujemy Home na folder Tomcat'a
7. Deployment -> zielony plus -> External Source -> wskazujemy na borowik.war
8. Aplikacje uruchamiamy najpierw budujac na konfiguracji Gradle, a potem deploy'ujac na serwer Tomcat'a

Konfigurowanie serwera FileZilla

1. Sciagamy serwer FileZilla https://filezilla-project.org/download.php?type=server i instalujemy na dysku
2. Przy uruchamianiu pozostawiamy wszystko domyslnie (host: localhost, port: 14147) i klikamy connect
3. Edit -> Users -> Add -> wpisujemy 'user' i klikamy OK
4. Zaznaczamy Password i wpisujemy 'pass'
5. Po lewej Page -> Shared folders -> Add -> dodajemy folder ze zdjeciami pasazerow podpisanymi "<imie> <nazwisko>.jpg" i klikamy OK

Stos technologiczny:

- Repozytorium kodu:
	- Bitbucket (https://bitbucket.org/pik2018/borowik/src)
- Śledzenie błędów / zarządzanie projektem:
	- JIRA (http://35.229.125.207:8080)
- Budowanie i deployment:
	- Jenkins (http://ec2-18-197-166-28.eu-central-1.compute.amazonaws.com:8080)
- Serwer:
    - Tomcat (http://ec2-18-197-166-28.eu-central-1.compute.amazonaws.com:9090)
- Konfiguracja projektu:
	- Gradle

Struktura projektu

![alt text](https://i.imgur.com/tJWQqJl.png)

Uzytkownik moze dodawac/usuwac/edytowac dany kurs, badz wyswietlac wszystkie kursy w liscie na dany dzien. Aplikacja komunikacje z uzytkownikiem realizuje poprzez pliki CSS + HTML oraz skrypty w JavaScript.
Sama aplikacja oparta jest na Springu wykorzystujac model MVC. Aplikacja pobiera zdjecia pasazerow przypisanych do danego przejazdu z serwera FTP, zas same przejazdy oraz listy dostepnych samochodow i kierowcow, pobiera z bazy danych MySQL poprzez Hibernate'a.
Aplikacja deploy'owana jest automatycznie budowana i deployowana na serwer Tomcat'a przez Jenkinsa, poprzez synchronizacje z repozytorium Bitbucketa.
Developerzy do zarzadzania zadaniami w projekcie wykorzytuja JIRE, a do dokonywania zmian w projekcie Intellij IDEA z wtyczka Gradle'a.

WEiTI PW 2018