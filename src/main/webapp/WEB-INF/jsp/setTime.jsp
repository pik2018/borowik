<!DOCTYPE HTML>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>


    <title>New Course</title>

    <link href="../../static/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../static/css/style.css" rel="stylesheet">

</head>
<body>

<div class="container text-center">
<h3>Add new course</h3>
<hr>

    <form:form class="form-horizontal" method="POST" modelAttribute="time" action="/${yearMonth}/${day}/setTime">
        <h5>Start time</h5>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="startHour">Hour</label>
                        <form:input id="sH" type="number" min="0" max="23" path="startHour" class="form-control"/>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="startMinute">Minute</label>
                        <form:input id="sM" type="number" min="0" max="59" path="startMinute" class="form-control"/>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>
        <h5>Finish time</h5>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="endHour">Hour</label>
                        <form:input id="eH" type="number" min="0" max="23" path="endHour" class="form-control"/>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="endMinute">Minute</label>
                        <form:input id="eM" type="number" min="0" max="59" path="endMinute" class="form-control"/>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-default">Next</button>
                </div>
            </div>
    </form:form>



</body>
</html>
