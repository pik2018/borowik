<!DOCTYPE HTML>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>


    <title>New Course</title>

    <link href="../../static/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../static/css/style.css" rel="stylesheet">
    <link href="../../static/css/newCourse.css" rel="stylesheet">

</head>
<body>

    <div class="container text-center">
        <h3>Add new course</h3>
            <hr>
            <form:form class="form-horizontal" modelAttribute="form" method="POST" action="/${yearMonth}/${day}/newCourse">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                <div class="form-group">
                    <label for="vipName">Vip name</label>
                    <form:input path="vipName" class="form-control"/>
                </div>
                    <div class="form-group">
                        <label for="vipSex">Vip sex</label>
                        <form:input path="vipSex" class="form-control"/>
                    </div>
                <div class="form-group">
                    <label for="driver">Driver</label>
                    <form:select path="driver" class="form-control">
                        <c:forEach var="driver" items="${drivers}">
                            <option>${driver.name}</option>
                        </c:forEach>
                    </form:select>
                </div>
                <div class="form-group">
                    <label for="car">Car</label>
                    <form:select path="car" class="form-control">
                        <c:forEach var="car" items="${cars}">
                            <c:choose>
                                <c:when test="${car.bulletproof == true}">
                                    <option class="uplighting">${car.model}  ${car.platesNumber}</option>
                                </c:when>
                                <c:otherwise>
                                    <option>${car.model}  ${car.platesNumber}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </form:select>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Save"/>
                </div>
                </div>
                <div class="col-md-3"></div>
            </div>
            </form:form>
        </div>


<script src="../../static/js/jquery-3.3.1.min.js"></script>
<script src="../../static/js/bootstrap.min.js"></script>
</body>
</html>