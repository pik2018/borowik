<!DOCTYPE HTML>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>


    <title>New Course</title>

    <link href="../../static/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../static/css/style.css" rel="stylesheet">

</head>
<body>
    <div class="container text-center">
        <h3>Course saved</h3>
        <hr>
        <form action="/${yearMonth}/${day}">
            <input class="btn-md btn-primary" type="submit" value="Ok" />
        </form>
    </div>
</body>
</html>
