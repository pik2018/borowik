<!doctype html>
<html lang="en">
<head>
    <title>Day View</title>
    <link href="../../static/css/calendar.css" rel="stylesheet">

    <script>
    function startTime() {
        var weekday = [];
        weekday[0] =  "Sunday";
        weekday[1] = "Monday";
        weekday[2] = "Tuesday";
        weekday[3] = "Wednesday";
        weekday[4] = "Thursday";
        weekday[5] = "Friday";
        weekday[6] = "Saturday";
        var month = [];
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        var d = today.getDate();
        var y = today.getFullYear();
        var wd = weekday[today.getDay()];
        var mt = month[today.getMonth()];

        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('date').innerHTML =
            d;
        document.getElementById('day').innerHTML =
            wd;
        document.getElementById('month').innerHTML =
            mt + "/" + y;

        var t = setTimeout(startTime, 500);
    }
    function checkTime(i) {
        if (i < 10) {
            i = "0" + i
        }
        return i;
    }
    function goTo(i) {
        location.href="201806/" + i;
    }
    </script>
</head>
<body onload="startTime()">
<div class="container">
    <div class="card">
        <div class="front">
            <div class="contentfront">
                <div class="month">
                    <table>
                        <tr class="orangeTr">
                            <th>Mon</th>
                            <th>Tue</th>
                            <th>Wed</th>
                            <th>Thu</th>
                            <th>Fri</th>
                            <th>Sat</th>
                            <th>Sun</th>
                        </tr>
                        <tr class="whiteTr">
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th onclick="goTo(1)">1</th>
                            <th onclick="goTo(2)">2</th>
                            <th onclick="goTo(3)">3</th>
                        </tr>
                        <tr class="whiteTr">
                            <th onclick="goTo(4)">4</th>
                            <th onclick="goTo(5)">5</th>
                            <th onclick="goTo(6)">6</th>
                            <th onclick="goTo(7)">7</th>
                            <th onclick="goTo(8)">8</th>
                            <th onclick="goTo(9)">9</th>
                            <th onclick="goTo(10)">10</th>
                        </tr>
                        <tr class="whiteTr">
                            <th onclick="goTo(11)">11</th>
                            <th onclick="goTo(12)">12</th>
                            <th onclick="goTo(13)">13</th>
                            <th onclick="goTo(14)">14</th>
                            <th onclick="goTo(15)">15</th>
                            <th onclick="goTo(16)">16</th>
                            <th onclick="goTo(17)">17</th>
                        </tr>
                        <tr class="whiteTr">
                            <th onclick="goTo(18)">18</th>
                            <th onclick="goTo(19)">19</th>
                            <th onclick="goTo(20)">20</th>
                            <th onclick="goTo(21)">21</th>
                            <th onclick="goTo(22)">22</th>
                            <th onclick="goTo(23)">23</th>
                            <th onclick="goTo(24)">24</th>
                        </tr>
                        <tr class="whiteTr">
                            <th onclick="goTo(25)">25</th>
                            <th onclick="goTo(26)">26</th>
                            <th onclick="goTo(27)">27</th>
                            <th onclick="goTo(28)">28</th>
                            <th onclick="goTo(29)">29</th>
                            <th onclick="goTo(30)">30</th>
                            <th></th>
                        </tr>
                    </table>
                </div>
                <div class="date">
                    <div class="datecont">
                        <div id="date"></div>
                        <div id="day"></div>
                        <div id="month"></div>
                        <i class="fa fa-pencil edit" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="back">
            <!---<div class="contentback">
              <div class="backcontainer">
                hhh
              </div>
            </div>--->
        </div>
    </div>
</div>
</body>
</html>
