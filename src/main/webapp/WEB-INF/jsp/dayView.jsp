<!doctype html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html lang="en">
<head>
    <title>Day View</title>

    <link href="../../static/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../static/css/style.css" rel="stylesheet">
</head>
<body>

<div class="container text-center">
    <td>
        <form method="POST">
            <div class="form-row">
                <div class="alignButtonLeft" style="width: 5%" name="backForm">
                    <input type="hidden" name="backButton">
                    <input type="button" class="btn-md btn-primary" value="Back" onclick="back()">
                </div>
                <div style="width: 82%">
                    <h3>Day view</h3>
                </div>
                <div class="alignButtonRight" style="width: 13%" name="addForm">
                    <input type="hidden" name="addButton">
                    <input type="button" class="btn-md btn-primary" value="Add new course" onclick="add()">
                </div>
            </div>
        </form>

        <script language="JavaScript">
            function back() {
                document.location='/';
            }

            function add() {
                document.location+='/setTime';
            }
        </script>
    </td>
    <table class="table table-striped">
        <tr>
            <th>ID</th>
            <th>Duration</th>
            <th>VIP name</th>
            <th>VIP photo</th>
            <th>Driver</th>
            <th>Car details</th>
            <th>VIP sex</th>
            <th></th>
            <th></th>
        </tr>
        <c:forEach var="course" items="${courses}" varStatus="loop">
            <tr>
                <td>${loop.index + 1}.</td>
                <c:set var="start" value="${course.startDate.toLocalDateTime().getMinute()}"/>
                <c:set var="finish" value="${course.finishDate.toLocalDateTime().getMinute()}"/>
                <c:if test="${start < 10 && start > 0}">
                    <c:set var="start" value="0${start}"/>
                </c:if>
                <c:if test="${start == 0}">
                    <c:set var="start" value="00"/>
                </c:if>
                <c:if test="${finish < 10}">
                    <c:set var="finish" value="0${finish}"/>
                </c:if>
                <c:if test="${finish == 0}">
                    <c:set var="finish" value="00"/>
                </c:if>

                <td>${course.startDate.toLocalDateTime().getHour()}:${start}
                    - ${course.finishDate.toLocalDateTime().getHour()}:${finish} </td>
                <td>${course.vipName}</td>
                <td><img src="ftp://user:pass@localhost/${course.vipName}.jpg" width="54" height="80"></td>
                <td>${course.driver.name}</td>
                <td>${course.car.model} ${course.car.platesNumber}</td>
                <td>${course.vipSex}</td>
                <td>
                    <form name="deleteForm" method="post">
                        <input type="hidden" name="deleteButton">
                        <input type="button" class="btn btn-primary btn-sm" value="Delete" onclick="del(${course.id})">
                    </form>
                </td>
                <td>
                    <form name="editForm" method="get">
                        <input type="hidden" name="editButton">
                        <input type="button" class="btn btn-primary btn-sm" value="Edit" onclick="edit(${course.id})">
                    </form>
                </td>
                <script language="JavaScript">
                    function edit(id) {
                        document.location='/edit/' + id;
                    }
                    function del(id) {
                        document.location+='/' + id + '/deleteCourse';
                    }
                </script>
            </tr>
        </c:forEach>
    </table>

</div>

</body>
</html>
