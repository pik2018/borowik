package Borowik.Repository;

import Borowik.Entity.Driver;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface DriverRepository extends JpaRepository<Driver, Long> {
    List<Driver> findAll();
    Driver findDriverByName(String name);
}
