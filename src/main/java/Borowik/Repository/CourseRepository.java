package Borowik.Repository;

import Borowik.Entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CourseRepository extends JpaRepository<Course, Long> {
    List<Course> findAll();
    List<Course> findAllByOrderByStartDate();
    void deleteById(long id);
    Course findByIdIs(long id);
}
