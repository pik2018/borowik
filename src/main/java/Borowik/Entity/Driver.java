package Borowik.Entity;


import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Drivers")
public class Driver {
    @Id
    @GeneratedValue (strategy=GenerationType.SEQUENCE)
    private long id;

    @Column(name = "Name")
    private String name;

    @OneToMany(mappedBy = "driver", fetch = FetchType.LAZY)
    private Set<Course> courses;

    public Driver() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }
}
