package Borowik.Entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Cars")
public class Car {
    @Id
    @GeneratedValue (strategy=GenerationType.SEQUENCE)
    private long id;

    @Column(name = "Model")
    private String model;

    @Column(name = "PlatesNumber")
    private String platesNumber;

    @Column(name = "Bulletproof")
    private boolean isBulletproof;

    @OneToMany(mappedBy = "car", fetch = FetchType.LAZY)
    private Set<Course> courses;

    public Car() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPlatesNumber() {
        return platesNumber;
    }

    public void setPlatesNumber(String platesNumber) {
        this.platesNumber = platesNumber;
    }

    public boolean isBulletproof() {
        return isBulletproof;
    }

    public void setBulletproof(boolean bulletproof) {
        isBulletproof = bulletproof;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }
}
