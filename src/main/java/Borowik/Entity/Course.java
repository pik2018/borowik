package Borowik.Entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "Courses")
public class Course {
    @Id
    @GeneratedValue (strategy=GenerationType.SEQUENCE)
    private long id;

    @Column(name = "Start_date")
    private Timestamp startDate;

    @Column(name = "Finish_date")
    private Timestamp finishDate;

    @Column(name = "Vip_name")
    private String vipName;

    @Column(name = "Vip_sex")
    private String vipSex;

    @ManyToOne
    @JoinColumn(name = "Driver_id", nullable = false)
    private Driver driver;

    @ManyToOne
    @JoinColumn(name = "Car_id", nullable = false)
    private Car car;

    public Course() {
    }

    public Course(Timestamp startDate, Timestamp finishDate, String vipName, Driver driver, Car car, String vipSex) {
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.vipName = vipName;
        this.vipSex = vipSex;
        this.driver = driver;
        this.car = car;
    }

    public Course(String vipName) {
        this.vipName = vipName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getVipName() {
        return vipName;
    }

    public void setVipName(String vipName) {
        this.vipName = vipName;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Timestamp finishDate) {
        this.finishDate = finishDate;
    }

    public String getVipSex() {
        return vipSex;
    }

    public void setVipSex(String vipSex) {
        this.vipSex = vipSex;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }
}
