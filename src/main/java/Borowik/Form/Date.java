package Borowik.Form;

public class Date {
    private int year;
    private int month;
    private String monthName;
    private int monthLength;
    private int numFirstDayOfMonthInWeek;
    private int day;
    private int today;

    public Date(int year, int month, String monthName, int monthLength, int numFirstDayOfMonthInWeek, int today) {
        this.year = year;
        this.month = month;
        this.monthName = monthName;
        this.monthLength = monthLength;
        this.numFirstDayOfMonthInWeek = numFirstDayOfMonthInWeek;
        this.today = today;
    }

    public Date(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public String getMonthName() {
        return monthName;
    }

    public int getMonthLength() {
        return monthLength;
    }

    public int getNumFirstDayOfMonthInWeek() {
        return numFirstDayOfMonthInWeek;
    }

    public int getToday() {
        return today;
    }

    public int getDay() {
        return day;
    }
}
