package Borowik.Form;


public class Form {
    private String vipName;
    private String car;
    private String driver;
    private String vipSex;

    public Form() {
    }


    public Form(String vipName, String car, String driver, String vipSex) {
        this.vipName = vipName;
        this.vipSex = vipSex;
        this.car = car;
        this.driver = driver;
    }

    public String getVipName() {
        return vipName;
    }

    public void setVipName(String vipName) {
        this.vipName = vipName;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getVipSex() {
        return vipSex;
    }

    public void setVipSex(String vipSex) {
        this.vipSex = vipSex;
    }
}