package Borowik.Controller;

import Borowik.Service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

@Controller
@RequestMapping("/{date}/{day}")
public class DayPageController {
    @Autowired
    private CourseService courseService;

    @RequestMapping(method = RequestMethod.GET)
    public String showDay(HttpServletRequest request, @PathVariable String date, @PathVariable int day) {
        int year = Integer.parseInt(date.substring(0, 4));
        int month = Integer.parseInt(date.substring(4, 6));
        request.setAttribute("courses", courseService.getDailyCourses(year, month, day));
        request.setAttribute("year", year);
        request.setAttribute("month", month);
        request.setAttribute("day", day);
        return "dayView";
    }

    @Transactional
    @RequestMapping(value = "/{courseId}/deleteCourse", method = RequestMethod.GET)
    public String deleteCourse(@PathVariable String date, @PathVariable int day, @PathVariable int courseId) {
        courseService.deleteCourseById(courseId);
        return "redirect:/" + date + '/' + day;
    }

}
