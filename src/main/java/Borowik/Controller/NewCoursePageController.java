package Borowik.Controller;

import Borowik.Entity.Car;
import Borowik.Entity.Course;
import Borowik.Form.Form;
import Borowik.Form.Time;
import Borowik.Service.CarService;
import Borowik.Service.CourseService;
import Borowik.Service.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


@Controller
@RequestMapping("/{yearMonth}/{day}")
public class NewCoursePageController {
    @Autowired
    private CarService carService;
    @Autowired
    private DriverService driverService;
    @Autowired
    private CourseService courseService;

    private Timestamp s;
    private Timestamp e;


    @RequestMapping(value = "/setTime", method = RequestMethod.GET)
    public String addTime(Model model, @PathVariable String day, @PathVariable String yearMonth) {
        model.addAttribute("yearMonth", yearMonth);
        model.addAttribute("day", day);
        model.addAttribute("time", new Time());
        return "setTime";
    }

    @RequestMapping(value = "/setTime", method = RequestMethod.POST)
    public String passTime(@ModelAttribute("time") Time time, RedirectAttributes redirectAttrs,
                           @PathVariable String day, @PathVariable String yearMonth) {
        if(time.getStartHour() > time.getEndHour() || (time.getStartHour() == time.getEndHour() && time.getStartMinute() >= time.getEndMinute()))
            return "redirect:/" + yearMonth + "/" + day;
        redirectAttrs.addAttribute("start", time.getStartHour() + "/" + time.getStartMinute());
        redirectAttrs.addAttribute("end", time.getEndHour() + "/" + time.getEndMinute());

        return "redirect:/" + yearMonth + "/" + day + "/newCourse";
    }

    @RequestMapping(value = "/newCourse", method = RequestMethod.GET)
    public String addCourse(@ModelAttribute("start") String start, @ModelAttribute("end") String end,
                            @PathVariable String day, @PathVariable String yearMonth, Model model) {
        s = getTime(yearMonth, day, start);
        e = getTime(yearMonth, day, end);
        model.addAttribute("yearMonth", yearMonth);
        model.addAttribute("day", day);
        model.addAttribute("form", new Form());
        model.addAttribute("cars", carService.getCarsAvailableInPeriodOfTime(s, e));
        model.addAttribute("drivers", driverService.getDriversAvailableInPeriodOfTime(s,e));
        return "newCourse";
    }

    @RequestMapping(value = "/newCourse", method = RequestMethod.POST)
    public String saveCourse(@PathVariable String day, @PathVariable String yearMonth,
                             @ModelAttribute("form") Form form, HttpServletRequest request){
        Course course = new Course(s,e, form.getVipName(), driverService.getDriverByName(form.getDriver())
                                    , findCar(form.getCar()), form.getVipSex());
        if(course.getVipName().equals("Jan Kowalski") && course.getCar().isBulletproof() == false){
            request.setAttribute("msg", "Jan Kowalski nie ma pancernego samochodu.");
            request.setAttribute("yearMonth", yearMonth);
            request.setAttribute("day", day);
            request.setAttribute("yearmonth", "Jan Kowalski nie ma pancernego samochodu.");
            return "notBulletproof";
        }
        courseService.save(course);
        request.setAttribute("yearMonth", yearMonth);
        request.setAttribute("day", day);
        return "savedCourse";
    }

    private Car findCar(String car){
        String platesNumber = car.substring(car.lastIndexOf(" ")+1);
        return carService.getCarByPlatesNumber(platesNumber);
    }

    private Timestamp getTime(String yearMonth, String day, String hourMinute){
        String year = yearMonth.substring(0, 4);
        String month = yearMonth.substring(4, 6);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd/H/m");
        Date date = null;
        try {
            date = dateFormat.parse( year+ "/" +month + "/" + day + "/" + hourMinute);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long time = date.getTime();
        return new Timestamp(time);
    }

}
