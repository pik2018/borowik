package Borowik.Controller;

import Borowik.Entity.Car;
import Borowik.Entity.Course;
import Borowik.Form.Date;
import Borowik.Form.Form;
import Borowik.Repository.CourseRepository;
import Borowik.Service.CarService;
import Borowik.Service.CourseService;
import Borowik.Service.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Controller
@RequestMapping("/edit")
public class EditController {
    @Autowired
    private CourseService courseService;
    @Autowired
    private CarService carService;
    @Autowired
    private DriverService driverService;

    private Timestamp finishDate;
    private Timestamp startDate;
    private long courseId;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String editById(@PathVariable int id, Model model) {
        Course c = courseService.findByIdIs(id);
        startDate = c.getStartDate();
        finishDate = c.getFinishDate();
        courseId = c.getId();
        model.addAttribute("form", new Form(c.getVipName(), c.getCar().getModel() + c.getCar().getPlatesNumber(), c.getDriver().getName(), ""));
        model.addAttribute("cars", carService.getCarsAvailableInPeriodOfTime(startDate, finishDate));
        model.addAttribute("drivers", driverService.getDriversAvailableInPeriodOfTime(startDate, finishDate));
        return "editForm";
    }
    private Car findCar(String car){
        String platesNumber = car.substring(car.lastIndexOf(" ")+1);
        return carService.getCarByPlatesNumber(platesNumber);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(Form f, HttpServletRequest request) {
        courseService.deleteCourseById(courseId);
        courseService.save(new Course(startDate, finishDate, f.getVipName(), driverService.getDriverByName(f.getDriver())
                , findCar(f.getCar()), ""));
        LocalDateTime localDateTime = startDate.toLocalDateTime();
        int year = localDateTime.getYear();
        int month = localDateTime.getMonthValue();
        int day = localDateTime.getDayOfMonth();
        return (String.valueOf(month).length() == 1) ? "redirect:/" + year + "0" + month + '/' + day
                : "redirect:/" + year + month + '/' + day;
    }
}
