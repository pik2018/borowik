package Borowik.Service;

import Borowik.Entity.Car;
import Borowik.Entity.Course;
import Borowik.Repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class CarService {
    private CarRepository carRepository;

    @Autowired
    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public Collection<Car> getAllCars() {
        return carRepository.findAll();
    }

    public Car getCarByPlatesNumber(String platesNumber) {return carRepository.findCarByPlatesNumber(platesNumber);}

    public Collection<Car> getCarsAvailableInPeriodOfTime(Timestamp start, Timestamp end) {
        List<Car> allCars = carRepository.findAll();
        List<Car> carsAvailable = new ArrayList<>();
        boolean isAvailable = true;

        for (Car car : allCars) {
            for (Course course : car.getCourses()) {
                if ((start.after(course.getStartDate()) && start.before(course.getFinishDate()))
                  || (end.after(course.getStartDate()) && end.before(course.getFinishDate()))
                    || (start.before(course.getStartDate()) && end.after(course.getFinishDate()))){
                    isAvailable = false;
                    break;
                }
            }
            if (isAvailable) {
                carsAvailable.add(car);
            } else {
                isAvailable = true;
            }
        }
        return carsAvailable;
    }
}
