package Borowik.Service;

import Borowik.Entity.Course;
import Borowik.Repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.*;

@Service
public class CourseService {
    @Autowired
    private CourseRepository courseRepository;

    public Collection<Course> getAllCourses() {
        return courseRepository.findAll();
    }

    private Collection<Course> getAllCourcesByStartDate() {
        return courseRepository.findAllByOrderByStartDate();
    }

    public Course findByIdIs(long id) {
        return courseRepository.findByIdIs(id);
    }

    public void save(Course course) {
        courseRepository.save(course);
    }

    public Collection<Course> getDailyCourses(int year, int month, int day) {
        List<Course> dailyCourses = new ArrayList<>();
        Collection<Course> allCourses = getAllCourcesByStartDate();
        boolean isCourseExist = false;
        for (Course c : allCourses) {
            LocalDate date = c.getStartDate().toLocalDateTime().toLocalDate();
            if (date.equals(LocalDate.of(year, month, day))) {
                dailyCourses.add(c);
                isCourseExist = true;
            } else if (isCourseExist)
                break;
        }
        return dailyCourses;
    }

    public void deleteCourseById(long id) {
        courseRepository.deleteById(id);
    }
}