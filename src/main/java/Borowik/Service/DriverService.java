package Borowik.Service;

import Borowik.Entity.Car;
import Borowik.Entity.Course;
import Borowik.Entity.Driver;
import Borowik.Repository.DriverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class DriverService {
    @Autowired
    private DriverRepository driverRepository;

    public Collection<Driver> getAllDrivers() {
        return driverRepository.findAll();
    }

    public Driver getDriverByName(String name) { return  driverRepository.findDriverByName(name);}

    public Collection<Driver> getDriversAvailableInPeriodOfTime(Timestamp start, Timestamp end) {
        List<Driver> allDrivers = driverRepository.findAll();
        List<Driver> driversAvailable = new ArrayList<>();
        boolean isAvailable = true;

        for (Driver driver: allDrivers) {
            for (Course course : driver.getCourses()) {
                if ((start.after(course.getStartDate()) && start.before(course.getFinishDate()))
                        || (end.after(course.getStartDate()) && end.before(course.getFinishDate()))
                        || (start.before(course.getStartDate()) && end.after(course.getFinishDate()))) {
                    isAvailable = false;
                    break;
                }
            }
            if (isAvailable) {
                driversAvailable.add(driver);
            } else {
                isAvailable = true;
            }
        }
        return driversAvailable;
    }
}
